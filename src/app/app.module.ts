import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { FirebaseProvider } from './../providers/firebase/firebase';
import { HttpClientModule, HttpClient } from '@angular/common/http'; 
// import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ReportPage } from '../pages/report/report'; 
import { MapPage } from '../pages/map/map';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AgmCoreModule } from '@agm/core';       
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { UserserviceProvider } from '../providers/userservice/userservice';
import { IonicStorageModule } from '@ionic/storage'
import { Facebook } from '@ionic-native/facebook';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';
import { File } from '@ionic-native/file';
// import { Storage } from '@ionic/storage';
const firebaseConfig = { 
  apiKey: "AIzaSyCnK_2bEp9ADimZFZE5euBxG0xoNrzzzBw",
  authDomain: "nabiabot.firebaseapp.com",
  databaseURL: "https://nabiabot.firebaseio.com",
  storageBucket: "nabiabot.appspot.com",
  messagingSenderId: "161378552420"
};   
declare var require: any; 
var firebase = require("firebase"); 
firebase.initializeApp(firebaseConfig);
@NgModule({   
  declarations: [
    MyApp,
    HomePage, 
    ReportPage,
    MapPage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp), 
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw'
    }),   
    HttpClientModule,
    // IonicStorageModule.forRoot({
    //   name: '__mydb', 
    //   driverOrder: ['indexeddb', 'sqlite', 'websql']
    // })
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp], 
  entryComponents: [
    MyApp, 
    HomePage, 
    ReportPage,
    MapPage,
    LoginPage, 
    RegisterPage 
  ], 
  providers: [
    StatusBar, 
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider,
    Geolocation,
    // ImagePicker,
    GoogleMaps,
    UserserviceProvider,
    Facebook, 
    Camera,
    Diagnostic,
    Network,
    File
    // Storage
  ]
  // ,
  // exports: [
  //   Geolocation
  // ]
})
export class AppModule {}
