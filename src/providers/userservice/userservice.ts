import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { FirebaseProvider } from '../firebase/firebase';
/*
  Generated class for the UserserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
declare var require: any; 
var firebase = require("firebase");
declare var FirebasePlugin: any; 
@Injectable()
export class UserserviceProvider {
  user: any;
  accountId: string;
  consentOverlay: boolean = false;
  constructor(public fb: FirebaseProvider, public http: HttpClient, private storage: Storage) {
    //console.log('Hello UserserviceProvider Provider');
  }

  reloadInfo(hasInfoCB, needsLoginCB){
    this.storage.get('user').then((user) => {
      if(user && user != undefined){
        this.user = JSON.parse(user);
        this.storage.get('accountId').then((id) => {
          if(id && id != undefined){
            this.accountId = JSON.parse(id);
          }
          
          console.log(JSON.stringify(this.user));

          if(this.user && this.accountId){
            if(this.user.consent){
              this.updateFBTokens();
              hasInfoCB(false);
            }else{
              hasInfoCB(true);
            }
          }else{
            needsLoginCB();
          }
        });
      }else{
        needsLoginCB();
      }
    });
  }

  updateConsent(consent: boolean){
    this.fb.updateConsent(this.accountId, consent);
  }

  updateFBTokens(){
    FirebasePlugin.getToken(token => {
      console.log(token)
      if(token && token != ""){
        this.fb.updateDeviceToken(this.accountId, token);
      }
      FirebasePlugin.subscribe('all');
    }, error => {
      console.log(JSON.stringify(error))
      console.error(`Error: ${error}`);
    });
    FirebasePlugin.onTokenRefresh(token => {
      console.log(token)
      if(token && token != ""){
        this.fb.updateDeviceToken(this.accountId, token);
      }
    }, function(error) {
      console.error(`Error: ${error}`);
    });
  }

  setUser(user, accountId){
    this.user = user;
    this.accountId = accountId;

    this.storage.set('user', JSON.stringify(user));
    this.storage.set('accountId', JSON.stringify(accountId));
    console.log("Setting up token");
      FirebasePlugin.getToken(token => {
        console.log(token)
        if(token && token != ""){
          this.fb.updateDeviceToken(accountId, token);
        }
        FirebasePlugin.subscribe('all');
      }, error => {
        console.log(JSON.stringify(error))
        console.error(`Error: ${error}`);
      });
      FirebasePlugin.onTokenRefresh(token => {
        console.log(token)
        if(token && token != ""){
          this.fb.updateDeviceToken(accountId, token);
        }
      }, function(error) {
        console.error(`Error: ${error}`);
      });
    console.log(this.user);
    console.log(this.accountId);
  }

  getUser(){
    return this.user;
  }

  getAccountId(){
    return this.accountId;
  }


  logout(cb){
    this.storage.set('user', null);
    this.storage.set('accountId', JSON.stringify(null));    
    firebase.auth().signOut();
    cb();
  }
}
