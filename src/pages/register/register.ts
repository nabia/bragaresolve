import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { ReportPage } from '../report/report';
import { LoginPage } from '../login/login';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var require: any; 
var firebase = require("firebase"); 
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  register: any;
  termsAndConditionsChecked: boolean = false;
  failureCover: boolean = false;
  consentOverlay: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private fb: FirebaseProvider, public fbuilder: FormBuilder, private us: UserserviceProvider) {
    this.register = fbuilder.group({
      'firstName' : [null,Validators.compose([Validators.required])],
      // 'lastName' : [null,Validators.compose([Validators.required])],
      'email' : [null,Validators.compose([Validators.required, Validators.email])],
      'password' : [null,Validators.compose([Validators.required,Validators.minLength(6)])],
      'confirmPassword' : [null,Validators.compose([Validators.required])]
      // 'telephone' : [null,Validators.compose([Validators.required])],
      // 'address' : [null,Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  toggleTerms(){
    this.termsAndConditionsChecked = !this.termsAndConditionsChecked;
    console.log(this.termsAndConditionsChecked)
    console.log(!this.register.valid || !this.termsAndConditionsChecked)
  }
  goToLogin(){
    console.log("navigateToLogin");
    this.navCtrl.setRoot(LoginPage);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.register.controls) {
        this.register.controls[c].markAsTouched();
    }
    console.log(value);
    var that = this;
    firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
    .then(function(firebaseUser) {
      console.log(firebaseUser);
      var userInstance = {
        firstName: value.firstName,
        lastName: "",
        photo: "assets/imgs/default-operator.png",
        phoneNumber: null,
        email: value.email,
        address: null,
        facebookId: null
      }
      that.fb.addUser(firebaseUser.uid, userInstance);
      that.us.setUser(userInstance, firebaseUser.uid);
      that.goToReport();
    })
    .catch(function(error) {
      console.log(JSON.stringify(error));
      if(error.code == "auth/email-already-in-use"){
          this.failureCover = true;
      }
    }); 
  }

  goToReport(){
    this.navCtrl.push(ReportPage);
  }

  showTerms(){
    this.consentOverlay = true;
  }

  dismissCover(){
    this.failureCover = false;
  }

  dismissTerms(){
    this.consentOverlay = false;
  }
}
