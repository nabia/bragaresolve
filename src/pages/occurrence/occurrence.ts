import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';


declare var require: any; 
var EXIF = require('exif-js');
/**
 * Generated class for the OccurrencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-occurrence',
  templateUrl: 'occurrence.html',
})
export class OccurrencePage {
  occurrence: any;
  timeAgo: string;
  occurrenceId: string;
  colorByState: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OccurrencePage');
    this.occurrence = this.navParams.get('data');
    // var img2 = document.getElementById("pic2");
    // console.log(img2);
    // EXIF.getData(img2, function() {

    //     var allMetaData = EXIF.getAllTags(this);
    //     var allMetaDataSpan = document.getElementById("allMetaDataSpan");
    //     allMetaDataSpan.innerHTML = JSON.stringify(allMetaData, null, "\t");

    // });
    this.occurrenceId = this.getReadableId(this.occurrence.key);
    this.colorByState = "red";
    this.timeAgo = this.getReadeableLastUpdate(this.occurrence.modifiedDate);


    // var img2 = document.getElementById("pic2");
    // console.log(img2);
    // EXIF.getData(img2, function() {
    //     var allMetaData = EXIF.getAllTags(this);
    //     var allMetaDataSpan = document.getElementById("allMetaDataSpan");
    //     allMetaDataSpan.innerHTML = JSON.stringify(allMetaData, null, "\t");
    // });
  }

  public backModal(){
    this.viewCtrl.dismiss(); 
  }

  public closeModal(){
    this.viewCtrl.dismiss(); 
  }

  getReadeableLastUpdate(timestamp){
    var now = new Date();
    var date = new Date(timestamp);
    var a = now, b = date;
    a.setHours(0,0,0,0);
    b.setHours(0,0,0,0);
    var months = ["Jan", "Fev", "Abr", "Mar", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
    if(a == b) {
      return 
    }else{
      return date.getDate()+" "+ months[date.getMonth()] +" "+ date.getFullYear();
    }
  }


  getIconByState(state){
    var imgs = [
      'assets/imgs/nabia-ocorrencias-por-resolver-icon.png',
      'assets/imgs/nabia-ocorrencias-atribuida-para-resolucao-icon.png',
      'assets/imgs/nabia-ocorrencias-aguarda-validacao-icon.png',      
      'assets/imgs/nabia-ocorrencias-resolvido-icon.png'
    ];


    return imgs[state];
  }

  getReadableId(key){
    var hash = key.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
    return hash < 0 ? hash * -1 : hash;
  }
}
