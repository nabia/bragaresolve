import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { ReportPage } from '../report/report';
import { Geolocation } from '@ionic-native/geolocation';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { HttpClient } from '@angular/common/http';
import { Modal, ModalController, ModalOptions } from 'ionic-angular';
import { OccurrencePage } from '../occurrence/occurrence';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { SplashScreen } from '@ionic-native/splash-screen';
declare var require: any; 
var firebase = require("firebase"); 
// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker
//  } from '@ionic-native/google-maps';
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  lat: number = 41.5454486;
  lng: number = -8.426506999999999;
  localities: any[];
  occurrences: any[];
  toggleFilterClass: boolean = false;
    // map: GoogleMap;
  user: any;
  accountId: string;
  types: string[] = ["Estradas, passeios e outra vias", "Equipamentos públicos", "Iluminação", "Jardins, árvores e outra vegetação", "Limpeza", "Mobiliário urbano", "Trânsito (sinalização vertical e horizontal)", "Outros", "Qualquer tipo"];
  selectedType: number = 8;
  searchParameter: string = null;

  constructor(public splashScreen: SplashScreen, public navCtrl: NavController, public navParams: NavParams, private fb: FirebaseProvider, public modalCtrl: ModalController, public menu: MenuController, public http: HttpClient/*, private googleMaps: GoogleMaps*/, private us: UserserviceProvider) {
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();
    this.fb.getLocalitiesByMunicipality().valueChanges().subscribe(result => {
      this.localities = result;
      // console.log(this.localities);
    });

    this.fb.getOccurrences().snapshotChanges().subscribe(result => {
      var occurrences = [];
      for(let occ of result){
        var temp = occ.payload.val();
        temp.key = occ.key;
        occurrences.push(temp);
      }
      this.occurrences = occurrences;
      //console.log(this.occurrences)
    });
    
    
  }
   
  ionViewDidLoad() {

    console.log('ionViewDidLoad MapPage');
    // this.loadMap();
  }
  navigateToReport(){
     this.menu.enable(false);
    // // this.navCtrl.setRoot(ReportPage);
     var that = this;
    // this.us.reloadInfo(
    //   function(){
    //     that.navCtrl.setRoot(ReportPage);
    //   },
    //     function(){
    //     that.navCtrl.setRoot(LoginPage);
    //   },
    // );
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        var x = that;
        console.log("THERE IS AUTH")
        that.us.reloadInfo(
          function(){
            x.navCtrl.setRoot(ReportPage);
          },
            function(){
              x.navCtrl.setRoot(LoginPage);
          },
        );
      } else {
        // No user is signed in.
        // console.log("NO AUTH BRO")
        that.navCtrl.setRoot(LoginPage);
      }
    });
   // this.openModal(this.occurrences[0]);
  }

  selectOnChange(event){
    // console.log(event)
    var addressQuery = event + " Braga";
    this.getGeocode(addressQuery).subscribe(result =>{
      if(result["status"] === "OK"){
        console.log(result["results"][0].geometry.location)
        this.lat = result["results"][0].geometry.location.lat;
        this.lng = result["results"][0].geometry.location.lng;
        // this.map.setOptions({
        //   camera: {
        //     target: {
        //       lat: this.lat,
        //       lng: this.lng
        //     }
        //   }
        // });
      }else{
        //handle error case
      } 
    })
  }

  getGeocode(address){
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw";
    return this.http.get(url);
  }

  // navigateToOccurrence(occurrence){ 
  //   console.log(occurrence)
  // }

//   loadMap() {
// console.log("should be loading map")
//     let mapOptions: GoogleMapOptions = {
//       camera: {
//         target: {
//           lat: this.lat,
//           lng: this.lng
//         },
//         zoom: 15,
//         tilt: 30
//       }
//     };

//     this.map = GoogleMaps.create('map', mapOptions);

//     // Wait the MAP_READY before using any methods.
//     this.map.one(GoogleMapsEvent.MAP_READY)
//       .then(() => {
//         console.log('Map is ready!');

//         for(var i = 0; i < this.occurrences.length; i++){
//           var occurrence = this.occurrences[i];
//           this.map.addMarker({
//               // title: 'Intervenção',
//               icon: 'red',
//               animation: 'DROP',
//               position: { 
//                 lat: occurrence.location.lat,
//                 lng: occurrence.location.lng
//               }
//             })
//             .then(marker => {
//               marker.on(GoogleMapsEvent.MARKER_CLICK)
//                 .subscribe(() => {
//                   console.log("click")
//                   this.openModal(occurrence);
//                 });
//             });
//         }
//         // Now you can use all methods safely.

//       });
//   }

getIcon(occurrence){
  var icons= [
    'assets/imgs/nabia-por-resolver-icon.png',
    'assets/imgs/nabia-atribuida-para-resolucao-icon.png',
    'assets/imgs/nabia-aguarda-validacao-icon.png',
    'assets/imgs/nabia-resolvida-icon.png',
  ]
  return icons[occurrence.state];
}
  openModal(occurrence){
    console.log(occurrence); 
    //occurrence as argument
    const myModal: Modal = this.modalCtrl.create('OccurrencePage', { data: occurrence });

    myModal.present();

    myModal.onDidDismiss((data) => { 
      console.log("modal went gone")
    });
  }

  toggleFilters(){
    this.toggleFilterClass = !this.toggleFilterClass;
  }

  logout(){
    var that = this;
    this.splashScreen.show();
    this.us.logout(function(){  
      // that.navCtrl.setRoot(HomePage); 
      location.reload();
    });
  }

  typeFilterOnChange($event){
    console.log(this.types.indexOf($event))
    this.selectedType = this.types.indexOf($event);
  }

  displayOccurrence(occurrence){
    return ((this.selectedType == 8) || (this.selectedType == parseInt(occurrence.type))) && (this.searchParameter == "" || this.searchParameter == null || occurrence.description.toLowerCase().indexOf(this.searchParameter.toLowerCase()) != -1);
  }
}
  