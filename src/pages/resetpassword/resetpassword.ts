import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
/**
 * Generated class for the ResetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var require: any; 
var firebase = require("firebase"); 
@IonicPage()
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {
  reset: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public fbuilder: FormBuilder) {
    this.reset = fbuilder.group({
      'email' : [null,Validators.compose([Validators.required, Validators.email])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }

  sendResetRequest($ev, value){
    try{
      firebase.auth().sendPasswordResetEmail(value.email);
    }catch(e){
      console.log(JSON.stringify(e));
    }

    this.viewCtrl.dismiss();
  }

  public backModal(){
    this.viewCtrl.dismiss(); 
  }

  public closeModal(){
    this.viewCtrl.dismiss(); 
  }
}
