import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
// import { Diagnostic } from '@ionic-native/diagnostic';

/**
 * Generated class for the MapModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map-modal',
  templateUrl: 'map-modal.html'
})
export class MapModalPage {
  lat: number = 41.5454486;
  lng: number = -8.426506999999999;
  latMarker: number = 41.5454486;
  lngMarker: number = -8.426506999999999;
  customStyle;
  // confirmBtnEnabled: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, private geolocation: Geolocation) {
    this.latMarker = this.lat;
    this.lngMarker = this.lng;
    this.customStyle = [{
      featureType: "poi.business",
      elementType: "labels",
      stylers: [{ visibility: "off" }]
    }];
    console.log("It got here");
    //this.diagnostic.isLocationAvailable().then(() => {
      this.geolocation.getCurrentPosition().then((resp) => {
        console.log(JSON.stringify(resp))
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
        this.latMarker = this.lat;
        this.lngMarker = this.lng;
        // this.confirmBtnEnabled = true;
       }).catch((error) => {
         console.log('Error getting location', error);
         console.log(JSON.stringify(error))
       });
    // }).catch(function(){
    //   console.log("No gps available")
    // });
  }



  mapClicked(event){
    this.latMarker = event.coords.lat;
    this.lngMarker = event.coords.lng;
  }
  ionViewDidLoad() {
    //const coords = this.navParams.get('coords');
  }

  public backModal(){
    this.viewCtrl.dismiss(); 
  }

  public closeModal(){
    const coords = {
      lat: this.latMarker,
      lng: this.lngMarker
    };
    this.viewCtrl.dismiss(coords); 
  }
}
  