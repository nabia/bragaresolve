import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapModalPage } from './map-modal';
import { AgmCoreModule } from '@agm/core/core.module';

@NgModule({
  declarations: [
    MapModalPage,
  ],
  imports: [
    IonicPageModule.forChild(MapModalPage),
    AgmCoreModule 
  ],
  exports: [
    MapModalPage,
  ]
})
export class MapModalPageModule {}
