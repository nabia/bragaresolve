import { Component } from '@angular/core';
import { NavController, NavParams,MenuController, Platform,normalizeURL } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Modal, ModalController, ModalOptions } from 'ionic-angular';
import { MapModalPage } from '../map-modal/map-modal';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient } from '@angular/common/http';
// import { ImagePicker } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MapPage } from '../map/map';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { Diagnostic } from '@ionic-native/diagnostic';
import { SplashScreen } from '@ionic-native/splash-screen';
/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 
 
// @IonicPage()

declare var require: any; 
var firebase = require("firebase"); 
declare var window: any
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  localities: any[] = null;
  types: string[] = ["Estradas, passeios e outra vias", "Equipamentos públicos", "Iluminação", "Jardins, árvores e outra vegetação", "Limpeza", "Mobiliário urbano", "Trânsito (sinalização vertical e horizontal)", "Outros"];
 // address: string;
  occurrence: any;
  lat: number;
  lng: number;
  reportSuccess: boolean = false;
  reportFailed: boolean = false;
  user: any;
  accountId: string;
  uploading: boolean = false;
  uploadType: number = 1;
  storeId: string = "289549584765897";
  base64Image: string = null;
  coords: any;
  hasMedia = true;
  base64ImageFinal: any;
  constructor(public platform: Platform, public splashScreen: SplashScreen, public diagnostic: Diagnostic, public navCtrl: NavController, private fbuilder: FormBuilder, public navParams: NavParams, private fb: FirebaseProvider, public modalCtrl: ModalController, private geolocation: Geolocation, private http: HttpClient, /*private imagePicker: ImagePicker,*/ private menu: MenuController, private us: UserserviceProvider, private camera: Camera) {
    this.user = this.us.getUser();
    this.accountId = this.us.getAccountId();
    this.fb.getLocalitiesByMunicipality().valueChanges().subscribe(result => {
      this.localities = result;

      // this.geolocation.getCurrentPosition().then((resp) => {
      //     this.coords.lat = resp.coords.latitude;
      //     this.coords.lng = resp.coords.longitude;

      //     this.localities.unshift({
      //       locality:"Usar posição atual"
      //     });
      // }).catch((error) => {
      //   console.log('Error getting location', error);
      // });

      this.localities.unshift({
        locality:"Usar posição atual"
      });

      this.localities.unshift({
        locality:"Escolher posição no mapa"
      });
    });

    this.occurrence = fbuilder.group({
      'description' : [null,Validators.compose([Validators.required])],
      'location' : [null,Validators.compose([Validators.required])],
      'address' : [null,Validators.compose([Validators.required])],
      'type' : [null,Validators.compose([Validators.required])],
      'file' : [null,null]
    });
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }


  openModal(){ 
    const coordsData = {
      lat: 0,
      lng: 0
    }
    const myModal: Modal = this.modalCtrl.create('MapModalPage', { coords: coordsData });
    myModal.present();
    myModal.onDidDismiss((coords) => {
      if(coords && coords != undefined){
        this.lat = coords.lat;
        this.lng = coords.lng;
        this.parseCoords(coords.lat, coords.lng).subscribe(data => {
          this.occurrence.controls['address'].setValue(data["results"][0].formatted_address);
        });
      }else{
        this.occurrence.controls['location'].setValue(null);
      }

    });
  }

  selectOnChange(event){
    if(event === this.localities[0].locality){
      this.openModal();
    }else if(event === this.localities[1].locality){
      if(this.platform.is("android")){
        let successCallback = (isAvailable) => { 
          if(!isAvailable){
            alert("Ligue o GPS para poder obter a sua localização");
          }else{
            this.geolocation.getCurrentPosition().then((resp) => {
              this.lat = resp.coords.latitude;
              this.lng = resp.coords.longitude;
              this.parseCoords(resp.coords.latitude, resp.coords.longitude).subscribe(data => {
                this.occurrence.controls['address'].setValue(data["results"][0].formatted_address);
              });
           }).catch((error) => {
             console.log('Error getting location', error);
           });
          }
        };
        let errorCallback = (e) => console.error(e);
        this.diagnostic.isGpsLocationEnabled().then(successCallback).catch(errorCallback);
      }else{
        this.geolocation.getCurrentPosition().then((resp) => {
          this.lat = resp.coords.latitude;
          this.lng = resp.coords.longitude;
          this.parseCoords(resp.coords.latitude, resp.coords.longitude).subscribe(data => {
            this.occurrence.controls['address'].setValue(data["results"][0].formatted_address);
          });
       }).catch((error) => {
         console.log('Error getting location', error);
         alert("Ligue o GPS para obter a sua localização.");
       });
      }

     }
  }

  parseCoords(lat, lng){
    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key=AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw";
    return this.http.get(url);
  }
  getGeocode(address){
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyDU37wpCZEr54JYeXQMtIokJgOLYu0DJlw";
    return this.http.get(url);
  }
  
  logForm(event, value){
    console.log(JSON.stringify(value))
    var now = new Date().getTime();
    if(this.platform.is("android")){
      var origin = 2;
    }else{
      var origin = 3;
    }
    var occurrence = {
      createdDate: now,
      modifiedDate: now,
      state: 0,
      locality: value.location,
      address: value.address,
      location: null,
      description: value.description,
      media: null,
      mediaType: "image",
      type: this.types.indexOf(value.type),
      createdBy: this.accountId, // insert current user id here
      createdByName: this.user.firstName + " " + this.user.lastName, // current user name here
      owner: null, // insert target operator id here
      ownerName: null, // insert target operator name here
      origin: origin,
      public: false,
      history: [
        {
          timestamp: now,
          desc: "Criação de ocorrência",
          userId: this.accountId
        }
      ]
    }

    if(occurrence.locality != this.localities[0].locality && occurrence.locality != this.localities[1].locality){
      var parsedLocality = occurrence.locality;
      if(occurrence.locality.indexOf("Freguesias") != -1){
        parsedLocality = occurrence.locality.substring(24);
      }
      var addressQuery = occurrence.address+" "+parsedLocality+" Braga";
      this.getGeocode(addressQuery).subscribe(result =>{
        if(result["status"] === "OK"){
          occurrence.location = result["results"][0].geometry.location;
          this.finalizeOccurrence(occurrence);
        }else{
          this.reportFailed = true;
        }
      }
      );
    }else{
      console.log(occurrence)
      occurrence.location = {
        lat: this.lat,
        lng: this.lng,
      }
      this.finalizeOccurrence(occurrence);
    }

  }

  finalizeOccurrence(occurrence){
    console.log("finalize occurrence called")
    if(this.base64Image){
      var sendOccurrence = this;
      occurrence.mediaType = 1;
      this.uploading = true;
      console.log(JSON.stringify(occurrence))
      this.upload(undefined,function(url) {
        console.log(JSON.stringify(occurrence))
        occurrence.media = url;
        sendOccurrence.fb.addOccurrence(occurrence); 
        sendOccurrence.uploading = false;
        sendOccurrence.reportSuccess = true;
      });
    }else{
      console.log(JSON.stringify(occurrence))
      this.fb.addOccurrence(occurrence); 
      this.reportSuccess = true;
    }
  }


  fileUpload(isCamera){
    console.log("Uploading file");
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.FILE_URI,
      allowEdit : true,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true,
      mediaType: this.camera.MediaType.PICTURE
    }

    if(!isCamera){
      options.sourceType = this.camera.PictureSourceType.SAVEDPHOTOALBUM;
    }

    if(this.platform.is("android")){
      options.destinationType= this.camera.DestinationType.DATA_URL;
    }

    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = imageData;
      if(this.platform.is("android")){
        this.base64ImageFinal = "data:image/jpeg;base64,"+imageData;
      }else{
        this.base64ImageFinal = normalizeURL(imageData);
      }

    }, (err) => {
    });
  }

  upload(hasDate,cb) {
    let storageRef = firebase.storage().ref();
    let success = false;
    let date: Date = hasDate != undefined? hasDate : new Date();
    var constructedDate = date.getFullYear().toString() + (date.getMonth()+1).toString() + date.getDate().toString();
    let contentFolder = "images";
    let elem_id = "file";
    let prefixOne = this.storeId;
    let prefixTwo = constructedDate;
    var name = (new Date()).getTime()+".jpeg";
    let path = `/occurrences/${contentFolder}/${prefixTwo}/${prefixOne}/${name}`;
    console.log(path)
    var iRef = storageRef.child(path);

    if(this.platform.is("android")){
      iRef.putString(this.base64Image, 'base64', {contentType:'image/jpeg'}).then((snapshot) => {
        console.log(snapshot.downloadURL)
        cb(snapshot.downloadURL);
      }, (error) => {
        console.log(JSON.stringify(error))
      });
    }else{
      window.resolveLocalFileSystemURL(this.base64Image, (fileEntry) => {
        fileEntry.file((resFile) => {

          var reader = new FileReader();
          reader.onloadend = (evt: any) => {

            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            iRef.put(imgBlob).then((snapshot) => {
              console.log(snapshot.downloadURL)
              cb(snapshot.downloadURL);
            });
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);

        });
      });
    }
  }

  navigateToMap(){
    this.menu.enable(false);
    this.navCtrl.setRoot(MapPage);
  }

  dismissCover(){
    this.reportFailed = false;
  }

  clearPhoto(){
    this.base64Image = null;
  }

  logout(){
    var that = this;
    this.splashScreen.show();
    this.us.logout(function(){
      // this.navCtrl.setRoot(HomePage);
      location.reload();
    });
  }
}
