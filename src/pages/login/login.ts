import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { ReportPage } from '../report/report';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Facebook } from '@ionic-native/facebook';
import { Modal, ModalController, ModalOptions } from 'ionic-angular';
import { ResetpasswordPage } from '../resetpassword/resetpassword';
import { SplashScreen } from '@ionic-native/splash-screen';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var require: any; 
var firebase = require("firebase"); 
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  login: any;
  loginFailedCover: boolean = false;
  fbLoginFailedCover: boolean = false;
  facebookLogin: boolean = false;
  getUserSubscribe: any = null;
  getOperatorSubscribe: any = null;
  consentOverlay: boolean = false;
  signInAction: number;
  userObj: any;
  userID: string;
  userPhoto: string;
  constructor(public splashScreen: SplashScreen, public navCtrl: NavController, public navParams: NavParams, private fb: FirebaseProvider, public fbuilder: FormBuilder, private us: UserserviceProvider, private facebook: Facebook, private platform: Platform, public modalCtrl: ModalController) {
    this.facebookLogin = false;
    this.login = fbuilder.group({
      'email' : [null,Validators.compose([Validators.required, Validators.email])],
      'password' : [null,Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  loginWithFacebook(){
    var provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().useDeviceLanguage();
    var that = this;
    this.facebookLogin = true;
    if (this.platform.is('cordova')) {
      this.facebook.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        
        firebase.auth().signInWithCredential(facebookCredential).then(function(result) {
          var user = result; 
          that.getUserSubscribe = that.fb.getUser(user.uid).valueChanges().subscribe(userDB => {
            console.log(JSON.stringify(userDB));
            console.log("cordova")
            if(!userDB){
              var name = user.providerData[0].displayName.split(" ");
              var userInstance = {
                firstName: name[0],
                lastName: name[name.length - 1],
                photo: user.providerData[0].photoURL,
                phoneNumber: user.providerData[0].phoneNumber,
                email: user.providerData[0].email,
                address: null,
                facebookId: user.providerData[0].uid,
                consent: true
              }
              that.userObj = userInstance;
              that.userID = user.uid;
              that.signInAction = 1; //fb cordova register
              that.consentOverlay = true;
            }else{
              if(userDB["consent"]){
                that.us.setUser(userDB, user.uid);
                that.fb.updateProfilePicture(user.uid, user.providerData[0].photoURL);
                that.goToReport();
              }else{
                that.userObj = userDB;
                that.userPhoto = user.providerData[0].photoURL;
                that.userID = user.uid;
                that.signInAction = 2; // fb login success with no consent
                that.consentOverlay = true;
              }              
            }
          });
        }).catch(function(error) {
          console.log(JSON.stringify(error))
          that.fbLoginFailedCover = true;
          this.facebookLogin = false;
        });;
      })
    }
    else {
      firebase.auth().signInWithPopup(provider).then(function(result) {
        this.facebookLogin = true;
        var user = result.user;
        that.getUserSubscribe = that.fb.getUser(user.uid).valueChanges().subscribe(userDB => {
          console.log(JSON.stringify(userDB));
          console.log("no cordova")
          if(!userDB){
            var name = user.providerData[0].displayName.split(" ");
            var userInstance = {
              firstName: name[0],
              lastName: name[name.length - 1],
              photo: user.providerData[0].photoURL,
              phoneNumber: user.providerData[0].phoneNumber,
              email: user.providerData[0].email,
              address: null,
              facebookId: user.providerData[0].uid
            }
            that.userObj = userInstance;
            that.userID = user.uid;
            that.signInAction = 1; //fb no cordova register
            that.consentOverlay = true;

          }else{
            if(userDB["consent"]){
              that.us.setUser(userDB, user.uid);
              that.fb.updateProfilePicture(user.uid, user.providerData[0].photoURL);
              that.goToReport();
            }else{
              that.userObj = userDB;
              that.userPhoto = user.providerData[0].photoURL;
              that.userID = user.uid;
              that.signInAction = 2; // login success with no consent
              that.consentOverlay = true;
            }
          }
        });
      }).catch(function(error) {
        console.log(JSON.stringify(error))
        that.fbLoginFailedCover = true;
        this.facebookLogin = false;
      });
    }

  }

  submitForm(ev, value){
    ev.preventDefault();
    for (let c in this.login.controls) {
        this.login.controls[c].markAsTouched();
    }
    if (this.login.valid) {
      var that = this;
      firebase.auth().signInWithEmailAndPassword(value.email,value.password)
      .then(function(user){
        //success
        console.log("Firebase auth success")
        var uid = user.uid;
        that.getUserSubscribe = that.fb.getUser(user.uid).valueChanges().subscribe(userDB => {
          console.log(uid)
          console.log(JSON.stringify(userDB))
          console.log("login")
          if(!userDB){
            that.getOperatorSubscribe = that.fb.getOperator(uid).valueChanges().subscribe(op => {
              var existingOperator = op[0];
              var userInstance = {
                firstName: existingOperator["firstName"],
                lastName: existingOperator["lastName"],
                photo: "assets/img/defaultProfile.png",
                phoneNumber: null,
                email: existingOperator["email"],
                address: null,
                facebookId: null,
                consent: true
              }
              that.userObj = userInstance;
              that.userID = user.uid;
              that.signInAction = 1; // login success with no user, user is operator first time
              that.consentOverlay = true;
            });
          }else{
            if(userDB["consent"]){
              that.us.setUser(userDB, user.uid);
              that.goToReport();
            }else{
              that.userObj = userDB;
              that.userID = user.uid;
              that.signInAction = 0; // login success with no consent
              that.consentOverlay = true;
            }

          }
        });
      })
      .catch(function(error) {
        that.loginFailedCover = true;
      });
    }
  }

  goToReport(){
    if(this.getUserSubscribe) this.getUserSubscribe.unsubscribe();
    if(this.getOperatorSubscribe) this.getOperatorSubscribe.unsubscribe();
    this.navCtrl.push(ReportPage);
  }

  dismissCover(){
    this.fbLoginFailedCover = false;
    this.loginFailedCover = false;
  }

  goToRecoverPassword(){
    this.openModal();
  }

  goToRegister(){
    this.navCtrl.setRoot(RegisterPage);
  }

  openModal(){
    //occurrence as argument
    const myModal: Modal = this.modalCtrl.create('ResetpasswordPage', { data: {} });

    myModal.present();

    myModal.onDidDismiss((data) => { 
    });
  }


  acceptTerms(){
    this.consentOverlay = false;
    switch(this.signInAction){
      case 0: 
              this.fb.updateConsent(this.userID, true);
              this.us.setUser(this.userObj, this.userID);
              this.goToReport();
              break;
      case 1: 
              this.fb.addUser(this.userID, this.userObj);
              this.us.setUser(this.userObj, this.userID);
              this.goToReport();
              break;

      case 2: 
              this.fb.updateConsent(this.userID, true);
              this.us.setUser(this.userObj, this.userID);
              this.fb.updateProfilePicture(this.userID, this.userPhoto);
              this.goToReport();      
              break;
      default: break;
    }
  }

  rejectTerms(){
    var that = this;
    this.splashScreen.show();
    this.us.logout(function(){  
      location.reload();
    });
  }

}
