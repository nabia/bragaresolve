import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ReportPage } from '../report/report';
import { MapPage } from '../map/map';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { LoginPage } from '../login/login';
import { Network } from '@ionic-native/network';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RegisterPage } from '../register/register';

declare var require: any; 
var firebase = require("firebase"); 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  firstTime: boolean = true;
  consentOverlay: boolean = false;
  constructor(public navCtrl: NavController, public us: UserserviceProvider,private network: Network,public splashScreen: SplashScreen) {
    this.firstTime = true;
  }
   
  navigateToReport(){
    var that = this;
    if(this.network.type){
      var authListenner =  firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          var x = that;
          if(that.firstTime){
            that.us.reloadInfo(
              function(needsConsent){
                //needs consent
                if(needsConsent){
                  x.consentOverlay = true;
                }else{
                  x.navCtrl.push(ReportPage);
                }
              }, 
              function(){
                x.navCtrl.push(LoginPage); 
              },
            );
          }
        } else {
          // No user is signed in.
          that.navCtrl.push(LoginPage);
        }
        that.firstTime = false;
      });    
    }else{
      alert("O dispositivo não tem ligação à internet.");
    }
  }

  navigateToMap(){
    //this.navCtrl.push(ReportPage);  
    if(this.network.type){
      this.navCtrl.push(MapPage);    
    }else{
      alert("O dispositivo não tem ligação à internet.");
    }
  }

  acceptTerms(){
    this.consentOverlay = false;
    this.us.updateFBTokens();
    this.us.updateConsent(true);
    this.navCtrl.push(ReportPage);
  }

  rejectTerms(){
    var that = this;
    this.splashScreen.show();
    this.us.logout(function(){  
      // that.navCtrl.setRoot(HomePage); 
      location.reload();
    });
  }
} 
 